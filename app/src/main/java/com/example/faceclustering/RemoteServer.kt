package com.example.faceclustering

import org.json.JSONObject
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.URL

val remoteAddress = "http://207.154.194.65:8086"

class RemoteServer {
    companion object {

        init {}

        fun getClusters(sentJSONObject: JSONObject): JSONObject {
            val t = khttp.get("$remoteAddress")
            val addr = "$remoteAddress/face_clustering"
            val sent_data = mapOf("json" to sentJSONObject.toString())
            val raw = khttp.post(addr, data = sent_data, timeout = 600.0)
            return raw.jsonObject
        }
    }
}