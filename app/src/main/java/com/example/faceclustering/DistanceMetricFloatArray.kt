package com.example.faceclustering;

import org.christopherfrantz.dbscan.DistanceMetric;
import kotlin.math.sqrt

class DistanceMetricFloatArray : DistanceMetric<FloatArray> {
    override fun calculateDistance(p0: FloatArray?, p1: FloatArray?): Double {
        var sum = 0.0
        for (i in 0..(p0?.size?.minus(1)!!)) {
            sum += (p0[i] - p1!![i]) * (p0[i] - p1[i])
        }
        return sqrt(sum)
    }
}
