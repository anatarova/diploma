package com.example.faceclustering

import android.graphics.Bitmap
import android.os.Environment
import org.tensorflow.Tensor
import org.tensorflow.contrib.android.TensorFlowInferenceInterface
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


class MobilenetModel(val tensorFlowInferenceInterface: TensorFlowInferenceInterface) : BaseModel() {
    private val graphInputName = "input_1"
    //private val graphInputName = "input_1"
    private val graphOutputName = "reshape_1/Reshape"
    //private val graphOutputName = "feats/Relu"
    private val imageSize = 192
    private val colorChannels = 3
    companion object {

        var ass = 0
    }
    fun normalizeImageBGR(bitmap: Bitmap): FloatArray {
        val w = bitmap.width
        val h = bitmap.height
        val floatValues = FloatArray(w * h * 3)
        val intValues = IntArray(w * h)
        bitmap.getPixels(intValues, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
        val imageMean = 127.5f
        val imageStd = 128f

        for (i in intValues.indices) {
            val `val` = intValues[i]
            floatValues[i * 3 + 2] = ((`val` shr 16 and 0xFF) - imageMean) / imageStd
            floatValues[i * 3 + 1] = ((`val` shr 8 and 0xFF) - imageMean) / imageStd
            floatValues[i * 3 + 0] = ((`val` and 0xFF) - imageMean) / imageStd
        }
        return floatValues
    }

    fun extract_features(boxes : Vector<Box>, bitmapPhoto: Bitmap) : FloatArray {
        var face_features = FloatArray(1024)
        for (box in boxes) {
            // todo: if zero
            val croppedBmp = Bitmap.createBitmap(bitmapPhoto,
                    box.left(), box.top(), box.width(), box.height())

            val pathToDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString()
            try {
                FileOutputStream(pathToDir + File.separator + "kukusiki" + ass.toString() + ".jpg").use({ out ->
                    croppedBmp.compress(Bitmap.CompressFormat.JPEG, 100, out) // bmp is your Bitmap instance
                    // PNG is a lossless format, the compression factor (100) is ignored
                })
            } catch (e: IOException) {
                e.printStackTrace()
            }
            ass++

            val resizedBitmap = Bitmap.createScaledBitmap(croppedBmp,
                    imageSize, imageSize, false)
            val netInput = normalizeImageBGR(resizedBitmap)
            tensorFlowInferenceInterface.feed(graphInputName, netInput, 1L, imageSize.toLong(), imageSize.toLong(), colorChannels.toLong())
            //tensorFlowInferenceInterface.feed("conv1_bn/keras_learning_phase", BooleanArray(1), 1L)
            val addFeed = tensorFlowInferenceInterface::class.java.getDeclaredMethod("addFeed", String::class.java, Tensor::class.java)
            addFeed.isAccessible = true
            addFeed.invoke(tensorFlowInferenceInterface,"conv1_bn/keras_learning_phase", Tensor.create(false, Boolean::class.javaObjectType))
            tensorFlowInferenceInterface.run(arrayOf(graphOutputName))
            tensorFlowInferenceInterface.fetch(graphOutputName, face_features)
        }
        return face_features
    }
}