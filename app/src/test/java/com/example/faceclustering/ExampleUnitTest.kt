package com.example.faceclustering

import org.hamcrest.CoreMatchers.instanceOf
import org.json.JSONObject
import org.junit.Test

import org.junit.Assert.*
import org.junit.internal.Classes.getClass
import java.io.File

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

//    @Test
//    fun json_for_server_wasCreated() {
//        val inferLocationActivity = InferLocationActivity()
//        var path_to_photos = ArrayList<String>()
//        val classLoader = javaClass.classLoader
//        val file = File(classLoader!!.getResource("johnny.jpg").file)
//        var t = file.absolutePath
//        path_to_photos.add(t)
//        path_to_photos.add(File(classLoader!!.getResource("kim.jpg").file).absolutePath)
//        val jsonArray = inferLocationActivity.createJSONForServer(path_to_photos)
//       // assertThat(jsonArray, instanceOf(JSONObject.class))
//        assertEquals(4, 2 + 2)
//    }
}
